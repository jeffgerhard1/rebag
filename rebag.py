directions = r'''Directions:

* RECOMMENDED: run the delete_thumbs.bat file in the targeted directory to delete all Thumbs.db files
* Then run this script like so:

>>> python rebag.py name_of_directory

The name of the directory can be a relative path (path\to\subdirectory).

This includes some added options:

>>> python rebag.py name_of_directory [checksum] [count]

The checksum is assumed to be md5 but you can make it sha1 or any other choice.
The count is a flag to try to count how many Bags are in the main directory. This takes a while.

So you could also run this like:

>>> python rebag.py path\to\subdirectory sha1 count

This program is fairly automatic but not 100% so. It will ask you to verify actions in a few cases:
- When there are extra items in the top-level directory
- When there are extra files found in the Bag data directory
- When there are missing files in the Bag (other than Thumbs.db files)

For cases of extra files found in the Bag, you will have to delete or move them manually for now... 
we can revisit this later.

'''

from pathlib import Path
import os
import bagit
import sys
from subprocess import Popen
from time import sleep

def rm_empty_directory(pth: Path):
    for child in pth.iterdir():
        if child.is_dir():
            rm_empty_directory(child)
        elif child.is_file():
            stray = pth / child
            print('\n\t** ALERT: Unexpected file found:\n\t  {}'.format(str(stray)))
            return False
    pth.rmdir()
    return True

def main_program(main_dir, checksum, count):
    if count is True:
        bags = main_dir.glob('**/bagit.txt')
        total = 0
        for b in bags:
            bag = b.parent.resolve()
            data_dir = bag / 'data'
            if data_dir.is_dir():
                total += 1
        print('Found {} Bags in {}'.format(total, main_dir.resolve()))
        input('Continue?')

    bags = main_dir.glob('**/bagit.txt')
    for b in bags:
        ignore = False
        bag = b.parent.resolve()
        data_dir = bag / 'data'
        if data_dir.is_dir():
            print('\nBag found: {}'.format(str(bag)))
            # print('\tData directory identified.')
            top_directory_files = list()  # keep a list because we can delete these
            for f in os.listdir(bag):
                if f == 'manifest-' + checksum + '.txt':
                    print('\t* Skipped: this Bag already has a {} checksum.'.format(checksum.upper()))
                    ignore = True
                elif f in ['bag-info.txt', 'bagit.txt']:
                    top_directory_files.append(f)
                elif f.startswith('manifest-') and f.endswith('.txt'):
                    top_directory_files.append(f)
                elif f.startswith('tagmanifest-') and f.endswith('.txt'):
                    top_directory_files.append(f)
                elif f != 'data':
                    unknown = bag / f
                    if unknown.is_dir():
                        print('\t**** UNEXPECTED DIRECTORY IN THIS BAG!')
                        print('\t     {}'.format(f))
                        input('\tThis will be moved to the data directory in the new Bag.')
                    elif unknown.is_file():
                        print('\tExtra file in top-level directory of Bag:\n\t {}'.format(f))
                        print('\tThis file will be moved to the data directory of the new Bag.')
                        print('\t(There is no clear alternative except to manually move this file out')
                        input('\tof the Bag main folder and then manually adding it back into the new Bag.)')
            if ignore is False:
                # print('\t{} bagit files found.'.format(len(top_directory_files)))
                # Let's see what's in the data directory
                # Or, maybe just read the manifest files???
                files = os.listdir(bag)
                manifest_found = False  # some Bags can have multiple manifests and we don't want to try to move files twice!
                for f in files:
                    if f.startswith('manifest-') and f.endswith('.txt'):  # this is probably good enough
                        if manifest_found is False:
                            manifest_found = True
                            manifest_file = bag / f
                            lines = manifest_file.read_text(encoding='utf-8').splitlines()
                            print('\tMoving the {} files/directories listed in the manifest...'.format(len(lines)))
                            # print('\tDEMO of file changes:')
                            for line in lines:
                                manifest_path = Path(line.split('  ')[1])
                                edited_path = Path(line.split('  ')[1].replace('data/', ''))
                                oldpath = bag / manifest_path
                                newpath = bag / edited_path
                                # IF WE SEE THUMBS.DB FILES, JUST DELETE 'EM
                                if line.endswith('humbs.db'):  # I think it's Thumbs with capital T but is it always??
                                    if oldpath.is_file():
                                        oldpath.unlink()
                                else:
                                    # print('\t\tMoving {} to:\n\t\t {}'.format(str(manifest_path), str(newpath)))
                                    # data_files_updater.append(tuple([oldpath, newpath]))
                                    if oldpath.is_file():
                                        newpath.parent.mkdir(parents=True, exist_ok=True)
                                        oldpath.rename(newpath)
                                    else:
                                        print('\t** MISSING FILE WARNING:\n\t {} is listed in the Bag manifest, but file is not found!'.format(manifest_path))
                                        answer = input('\tContinue? (There is not much we can do about this)')
                                        if len(answer) > 0 and answer[0].lower() == 'n':
                                            print('\n\n\tAll you can do is interrupt this rebagging program by hitting CTRL-C')
                                            input('\n\n\t(or hit enter to just continue...)')
                print('\tDeleting {} files from the top-level directory'.format(len(top_directory_files)))
                for file in top_directory_files:
                    to_del = bag / file
                    to_del.unlink()
                fix_strays = rm_empty_directory(data_dir)
                while fix_strays is False:
                    print('\n\tClean up stray files and empty (but don\'t delete) the data directory manually!')
                    input('\tYou can choose whether to delete the file or move it into the top-level directory.')
                    fix_strays = rm_empty_directory(data_dir)
                bag = bagit.make_bag(bag, checksum=[checksum])
                print('\tRebagged with {} checksum.'.format(checksum))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        count = False
        checksum = 'md5'
        # get the path
        main_dir = Path(sys.argv[1]).resolve()
        if len(sys.argv) > 2:
            checksum = sys.argv[2]

        if len(sys.argv) > 3:
            count = True
        main_program(main_dir, checksum, count)
    else:
        print(directions)


