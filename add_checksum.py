'''Add a new checksum to existing Bags.'''
directions = r'''USAGE INSTRUCTIONS:

>>> python add_checksum.py name_of_directory [checksum]

Put this file in a folder above the directory you want to scan for Bags,
or you can enter a relative path. Optionally, you can add a checksum,
but it will run md5 by default.

You can also run this with a relative path like this:

>>> python add_checksum.py path\to\subdirectory
'''

import bagit
import os
import sys
from pathlib import Path

def add_checksum(directory, checksum, processes=4):
    os.chdir(directory)
    total_bytes, total_files = bagit.make_manifests('data', processes=processes,
                                                    algorithms=[checksum])
    if total_files:
        print('Added {} manifest for {}'.format(checksum, directory))
    bagit._make_tagmanifest_file(checksum, '.', encoding='utf-8')

checksum = 'md5'
if len(sys.argv) > 2:
    checksum = sys.argv[2]
if len(sys.argv) > 1:
    # get the path
    main_dir = Path(sys.argv[1]).resolve()
    # go through the directory, find Bags, and add new checksum
    bagit_txts = main_dir.glob('**/bagit.txt')
    for b in bagit_txts:
        bag = b.parent.resolve()
        existing_manifest = bag / 'manifest-{}.txt'.format(checksum)
        if existing_manifest.is_file():
            print('* {} manifest already exists for {}'.format(checksum, bag))
        else:
            add_checksum(bag, checksum)

else:
    print(directions)