# Rebag functions #

For interoperability with cloud storage fixity checking, we have a need to rebag collections with md5 checksums. This code has three utilities:

1. `add_checksum.py`: Use this from the command line to add new manifests (presumably md5) to existing Bags.
2. `delete_thumbs.bat`: Drag this into a folder, run it, and it will process all folders and subfolders, seeking out and deleting any Thumbs.db files. Use with caution!!
3. `rebag.py`: Use this from the command line to unbag existing bags, then rebag them with the desired checksum. 

A general strategy would be to use the first file for a no-mess, quick solution; or to use the second and third files to do some cleaning up of Bags (like deleting Thumbs.db files) when you can do hands-on checking.

None of these files validates existing Bags, but you can use the existing bagit-python module to do that if you want.

All of these files work by scanning a top-level directory and then parsing the subdirectories inside of it. The Python files look for Bags in a basic manner, simply searching for "bagit.txt" files.


## Requirements ##

The [bagit-python module](https://github.com/LibraryOfCongress/bagit-python) must be installed.

## Usage ##

### add_checksum.py ###

`>>> python add_checksum.py name_of_directory`

Put this file in a folder above the directory you want to scan for Bags,
or you can enter the path. Optionally, you can add a checksum other than md5 (which is the default).

`>>> python add_checksum.py name_of_directory sha1` (or whatever other checksum you want)

You can also run this with a relative path like this:

`>>> python add_checksum.py path\to\subdirectory`

### delete_thumbs.bat ###

Code was borrowed from [this source](https://github.com/MarcusBarnes/mik/wiki/Cookbook:-Removing-.Thumbs.db-files-from-a-directory). Simply run the batch script and it will go through the present directory and all subdirectories, deleting Thumbs.db files.

### rebag.py ###

`>>> python rebag.py name_of_directory`

The name of the directory can be a relative path (path\to\subdirectory).

This includes some added options:

`>>> python rebag.py name_of_directory [checksum] [count]`

The checksum is assumed to be md5 but you can make it sha1 or any other choice.
The count is a flag to try to count how many Bags are in the main directory. This takes a while.

So you could also run this like:

`>>> python rebag.py path\to\subdirectory sha1 count`

This program is fairly automatic but not 100% so. It will ask you to verify actions in a few cases:
- When there are extra items in the top-level directory
- When there are extra files found in the Bag data directory
- When there are missing files in the Bag (other than Thumbs.db files)

For cases of extra files found in the Bag, you will have to delete or move them manually for now... 
we can revisit this later.